from flask_wtf import FlaskForm
from wtforms import SubmitField, StringField
from wtforms.ext.sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField
from wtforms.validators import DataRequired

from app import db
from app.models import User, Author


def usernames_choices():
    return db.session.query(User).all()


class LoanBookForm(FlaskForm):
    loan_to = QuerySelectField('loan to', validators=[DataRequired()],
                               query_factory=usernames_choices)
    submit = SubmitField()


class AuthorRegisterForm(FlaskForm):
    author_name = StringField('Author Name', validators=[
        DataRequired()
    ])
    submit = SubmitField('Register')


def author_choices():
    return db.session.query(Author).all()


class BookForm(FlaskForm):
    bookname = StringField('Book Name', validators=[
        DataRequired()
    ])
    author = QuerySelectMultipleField('Author', validators=[DataRequired()],
                               query_factory=author_choices)
    publisher = StringField('Publisher', validators=[
        DataRequired()
    ])
    submit = SubmitField('Register')
