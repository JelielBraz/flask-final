from flask import render_template, flash, redirect, url_for, request
from flask_login import login_required, current_user

from app import db
from app.decorators import permission_required
from app.main.forms import LoanBookForm, AuthorRegisterForm, BookForm
from app.models import Book, Permission, User, Role, Author, AuthorBook
from . import main


@main.route('/')
def index():
    return render_template('index.html')


@main.route('/list_author')
@login_required
def list_author():
    page = request.args.get('page', 1, type=int)
    pagination = Author.query.order_by(
        Author.name_author.asc()
    ).paginate(
        page, per_page=10, error_out=False
    )
    authors = pagination.items
    return render_template('list_authors.html', authors=authors, pagination=pagination)


@main.route('/user/<username>')
@login_required
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    user_logado = current_user
    page = request.args.get('page', 1, type=int)
    pagination = Book.query.filter_by(owner_user_id=user.id).order_by(
        Book.bookname.desc()
    ).paginate(
        page, per_page=10, error_out=False
    )
    books = pagination.items
    return render_template('user_profile.html', user=user, books=books, pagination=pagination, current_user=user_logado)


@main.route('/painel')
@login_required
def painel():
    user = User.query.filter_by(id=current_user.id).first_or_404()
    permission = Permission()
    return render_template('painel.html', user=user, permission=permission)


@main.route('/book_register', methods=['GET', 'POST'])
@permission_required(Permission.HAVE_BOOK)
@login_required
def book_register():
    form = BookForm()
    if form.validate_on_submit():
        book = Book(
            bookname=form.bookname.data,
            publisher=form.publisher.data,
            owner_user_id=current_user.id
        )
        db.session.add(book)
        db.session.commit()
        for i in range(len(form.author.data)):
            author_book = AuthorBook(
                id_book=book.id,
                id_author=form.author.data[i].id
            )
            db.session.add(author_book)
        db.session.commit()
        flash("Book registered")
        return redirect(url_for('main.list_books'))
    return render_template('book_register.html', form=form)


@main.route('/list_books')
@login_required
def list_books():
    page = request.args.get('page', 1, type=int)
    pagination = Book.query.order_by(
        Book.bookname.asc()
    ).paginate(
        page, per_page=10, error_out=False
    )
    books = pagination.items
    user = current_user
    return render_template('list_books.html', books=books, current_user=user, pagination=pagination)


@main.route('/loan_book/<id_book>', methods=['GET', 'POST'])
@login_required
def loan_book(id_book):
    form = LoanBookForm()
    if form.validate_on_submit():
        Book.query.filter_by(id=id_book).update(dict(loan_to=form.loan_to.data.id))
        db.session.commit()
        return redirect(url_for('main.list_books'))

    book = Book.query.filter_by(id=id_book).first_or_404()
    users = User.query.all()
    return render_template('loan_book.html', book=book, users=users, form=form)


@main.route('/returned_book/<id_book>', methods=['GET', 'POST'])
def returned_book(id_book):
    Book.query.filter_by(id=id_book).update(dict(loan_to=None))
    db.session.commit()
    return redirect(url_for('main.list_books'))


@main.route('/permission_update')
@permission_required(Permission.ADMIN)
@login_required
def permission_update():
    page = request.args.get('page', 1, type=int)
    pagination = User.query.paginate(
        page, per_page=10, error_out=False
    )
    users = pagination.items
    authenticated_user = current_user
    return render_template('permission_update.html',
                           users=users, current_user=authenticated_user, pagination=pagination)


@main.route('/change_permission/<id_user>/<permission>', methods=['GET'])
@login_required
@permission_required(Permission.ADMIN)
def change_permission(id_user=None, permission=None):
    if id_user and permission in ['Owner', 'Admin', 'User']:
        role = Role.query.filter_by(name=permission).first_or_404()
        User.query.filter_by(id=id_user).update(dict(role_id=role.id))
        db.session.commit()
        return redirect(url_for('main.permission_update'))
    return render_template('404.html')


@main.route('/author_register', methods=['POST', 'GET'])
@login_required
@permission_required(Permission.ADMIN)
def author_register():
    form = AuthorRegisterForm()
    if form.validate_on_submit():
        author = Author(name_author=form.author_name.data)
        db.session.add(author)
        db.session.commit()
        return redirect(url_for('main.painel'))
    return render_template('author_register.html', form=form)
