from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

from app import db

from . import login_manager


class Permission:
    GET_A_BOOK = 1
    HAVE_BOOK = 2
    ADMIN = 4


class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    default = db.Column(db.Boolean, default=False, index=True)
    permissions = db.Column(db.Integer)
    users = db.relationship('User', backref='role')

    def __init__(self, **kwargs):
        super(Role, self).__init__(**kwargs)
        if self.permissions is None:
            self.permissions = 0

    def __repr__(self):
        return '<Role %r>' % self.name

    def has_permission(self, perm):
        return self.permissions & perm == perm

    def add_permission(self, perm):
        if not self.has_permission(perm):
            self.permissions += perm

    def remove_permission(self, perm):
        if not self.has_permission(perm):
            self.permission -= perm

    def reset_permission(self):
        self.permission = 0

    @staticmethod
    def insert_roles():
        roles = {
            'User': [
                Permission.GET_A_BOOK
            ],
            'Owner': [
                Permission.GET_A_BOOK,
                Permission.HAVE_BOOK
            ],
            'Admin': [
                Permission.GET_A_BOOK,
                Permission.HAVE_BOOK,
                Permission.ADMIN
            ]
        }
        default_role = 'User'
        for r in roles:
            role = Role.query.filter_by(name=r).first()
            if role is None:
                role = Role(name=r)
            role.reset_permission()
            for perm in roles[r]:
                role.add_permission(perm)
            role.default = (role.name == default_role)
            db.session.add(role)
        db.session.commit()


class Book(db.Model):
    __tablename__ = "books"
    id = db.Column(db.Integer, primary_key=True)
    bookname = db.Column(db.String(64))
    publisher = db.Column(db.String(64))
    owner_user_id = db.Column(db.Integer(), db.ForeignKey('users.id'))
    loan_to = db.Column(db.Integer(), db.ForeignKey('users.id'))

    def __repr__(self):
        return '<Book %r>' %self.bookname


class AuthorBook(db.Model):
    __tablename__ = 'author_books'
    id = db.Column(db.Integer, primary_key=True)
    id_book = db.Column(db.Integer(), db.ForeignKey('books.id'))
    id_author = db.Column(db.Integer(), db.ForeignKey('author.id'))
    book = db.relationship('Book', backref='book_ref')

    def __repr__(self):
        return '%r' %self.id


class Author(db.Model):
    __tablename__ = 'author'
    id = db.Column(db.Integer, primary_key=True)
    name_author = db.Column(db.String(64), unique=True)
    author = db.relationship('AuthorBook', backref='author_ref')

    def __repr__(self):
        return self.name_author


class User(UserMixin, db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), unique=True, index=True)
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))
    password_hash = db.Column(db.String(128))
    books_owner = db.relationship(
        'Book',
        foreign_keys=[Book.owner_user_id],
        backref=db.backref('owner', lazy='joined'),
        lazy='dynamic',
        cascade='all, delete-orphan'
    )
    user_with_book = db.relationship(
        'Book',
        foreign_keys=[Book.loan_to],
        backref=db.backref('user_loan', lazy='joined'),
        lazy='dynamic',
        cascade='all, delete-orphan'
    )

    def __init__(self, **kwargs):
        super(User, self).__init__(**kwargs)
        if self.role is None:
            self.role = Role.query.filter_by(default=True).first()

    def __repr__(self):
        return self.username

    @property
    def password(self):
        raise AttributeError('Password is not readable')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def has_permission(self, perm):
        return self.role is not None and self.role.has_permission(perm)


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))
