from app import db
from app.models import Book, User, Author

users = [
    ['Jeliel', '123', 1],
    ['Evelyn', '123', 1],
    ['Kauan', '123', 1],
    ['MarcoAndre', '123', 1],
    ['Yuri', '123', 1],
    ['Christopher', '123', 1],
    ['Celio', '123', 1],
    ['Ivo', '123', 1],
    ['Joice', '123', 1],
    ['Erick', '123', 1],
    ['Edenir', '123', 1],
    ['Hartmann', '123', 1],
    ['Antonio', '123', 1],
    ['Gabriel', '123', 1],
    ['Luis', '123', 1],
    ['João', '123', 1],
    ['João', '123', 1],
    ['Ruivo', '123', 1],
    ['Russo', '123', 1],
]
for u in users:
    name, password, role_id = u[0], u[1], u[2]
    user = User.query.filter_by(username=name).first()
    if user is None:
        user = User(username=name, password=password, role_id=role_id)
        db.session.add(user)
db.session.commit()


authors = [
    ['J K Rolling'],
    ['John Flanagan'],
    ['Clarisse Lispector'],
    ['William Shakespeare'],
    ['J. R. R. Tolkien'],
    ['Clive Staples Lewis'],
    ['Rick Riordan'],
    ['Jorge Amado'],
    ['Carlos Drummond de Andrade'],
    ['Graciliano Ramos'],
    ['Lima Barreto'],
    ['Euclides da Cunha'],
    ['Machado de Assis']
]
for a in authors:
    name = a[0]
    author = Author.query.filter_by(name_author=name).first()
    if author is None:
        author = Author(name_author=name)
        db.session.add(author)
db.session.commit()

# user = User.query.filter_by(username='Jeliel').first()
# john_flanagam = Author.query.filter_by(name_author='John Flanagan').first()
# john_flanagam_id = john_flanagam.id
# books = [
#     ['Rangers a Ordem dos Arqueiros - Ruínas de Gorlan', john_flanagam_id, 'Fundamento', user.id],
#     ['Rangers a Ordem dos Arqueiros - Ponte em chamas', john_flanagam_id, 'Fundamento', user.id],
#     ['Rangers a Ordem dos Arqueiros - Terra do Gelo', john_flanagam_id, 'Fundamento', user.id],
#     ['Rangers a Ordem dos Arqueiros - Folha de Carvalho', john_flanagam_id, 'Fundamento', user.id],
#     ['Rangers a Ordem dos Arqueiros - Feiticeiro do Norte', john_flanagam_id, 'Fundamento', user.id],
#     ['Rangers a Ordem dos Arqueiros - Cerco à Macindaw', john_flanagam_id, 'Fundamento', user.id],
#     ['Rangers a Ordem dos Arqueiros - Resgate de Erak', john_flanagam_id, 'Fundamento', user.id],
#     ['Rangers a Ordem dos Arqueiros - Reis de Clonmel', john_flanagam_id, 'Fundamento', user.id],
#     ['Rangers a Ordem dos Arqueiros - Halt em Perigo', john_flanagam_id, 'Fundamento', user.id],
#     ['Rangers a Ordem dos Arqueiros - Imperador de Nihon-Ja', john_flanagam_id, 'Fundamento', user.id],
#     ['Rangers a Ordem dos Arqueiros - As historias perdidas', john_flanagam_id, 'Fundamento', user.id],
#     ['Rangers a Ordem dos Arqueiros - O Arqueiro do Rei', john_flanagam_id, 'Fundamento', user.id],
#     ['Rangers - A origem - Torneio de Gorlan', john_flanagam_id, 'Fundamento', user.id],
#     ['Rangers - A origem - Batalha de Hackhan', john_flanagam_id, 'Fundamento', user.id]
# ]
# for b in books:
#     name, author, publisher, id = b[0], b[1], b[2], b[3]
#     book = Book.query.filter_by(bookname=name).first()
#     if book is None:
#         book = Book(bookname=name, author_id=author, publisher=publisher, owner_user_id=id)
#         db.session.add(book)
# db.session.commit()



