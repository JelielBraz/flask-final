from flask import render_template, url_for, flash, redirect, request
from flask_login import login_user, logout_user, login_required

from app import db
from app.auth.forms import RegistrationForm, LoginForm
from app.models import User, Book
from . import auth


@auth.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is not None and user.verify_password(form.password.data):
            login_user(user)
            next = request.args.get('next')
            if next is None or not next.startswith('/'):
                next = url_for('main.painel')
            return redirect(next)
        flash('Invalid username or passoword.')
    return render_template('auth/login.html', form=form)


@auth.route('/user/register', methods=['POST', 'GET'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(
            username=form.username.data,
            password=form.password.data
        )
        db.session.add(user)
        db.session.commit()
        flash('Registered user')
        return redirect(url_for('auth.login'))
    return render_template('auth/register.html', form=form)


@login_required
@auth.route('/logout')
def logout():
    logout_user()
    flash('You are disconect now')
    return redirect(url_for('main.index'))


