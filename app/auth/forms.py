from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired, Length, equal_to, ValidationError

from app.models import User


class RegistrationForm(FlaskForm):
    username = StringField('User', validators=[
        DataRequired(), Length(1, 64),
    ])
    password = PasswordField('Password', validators=[
        DataRequired()
    ])
    password2 = PasswordField('Confirm the password', validators=[
        DataRequired(), equal_to('password', message='The passwords are not the same')
    ])
    submit = SubmitField('Create Account')

    def validate_username(self, field):
        if User.query.filter_by(username=field.data).first():
            raise ValidationError("User already exists")


class LoginForm(FlaskForm):
    username = StringField('User', validators=[
        DataRequired()
    ])
    password = PasswordField('Password', validators=[
        DataRequired()
    ])
    submit = SubmitField('Login')
